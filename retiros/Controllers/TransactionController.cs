﻿using Cordillera.Distribuidas.Event.Bus;
using depositos.DTOs;
using depositos.Messages.Commands;
using depositos.Service;
using depositos.Services;
using Microsoft.AspNetCore.Mvc;

namespace depositos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly IServiceTransaction _transactionService;
        private readonly IEventBus _bus;
        private readonly IServiceAccount _accountService;
        

        public TransactionController(IServiceTransaction transactionService, IEventBus bus, IServiceAccount accountService)
        {
            _transactionService = transactionService;
            _bus = bus;
            _accountService = accountService;
        }


        [HttpPost("Deposit")]
        public async Task<ActionResult> Deposit(TransactionRequest request)
        {
            Models.Transaction shir = new Models.Transaction()
            {
                AccountId = request.AccountId,
                Amount = request.Amount,
                CreationDate = DateTime.Now.ToShortDateString(),
                Type = "Deposit"
            };
            shir = await _transactionService.Withdraw(shir);
            bool isProccess = _accountService.Execute(shir);


            if (isProccess)
            {
                var transactionCreateCommand = new TransactionCreateCommand(
                   idTransaction: shir.Id,
                   amount: shir.Amount,
                   type: shir.Type,
                   creationDate: shir.CreationDate,
                   accountId: shir.AccountId
                );
                _bus.SendCommand(transactionCreateCommand);
            }

            return Ok(shir);


        }
    }
}
