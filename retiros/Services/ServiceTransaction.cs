﻿using depositos.Models;
using depositos.Repository;

namespace depositos.Service
{
    public class ServiceTransaction : IServiceTransaction
    {
        private readonly ContextDatabase _contextDatabase;

        public ServiceTransaction(ContextDatabase contextDatabase)
        {
            _contextDatabase = contextDatabase;
        }

        public async Task<Transaction> Withdraw(Transaction transactions)
        {
            _contextDatabase.Transaction.Add(transactions);
            await _contextDatabase.SaveChangesAsync();
            return transactions;
        }
    }
}
