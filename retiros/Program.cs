using Consul;
using Cordillera.Distribuidas.Discovery.Consul;
using Cordillera.Distribuidas.Discovery.Mvc;
using Cordillera.Distribuidas.Event;
using depositos.Data;
using depositos.Messages.CommandHandlers;
using depositos.Messages.Commands;
using depositos.Repository;
using depositos.Service;
using depositos.Services;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Nacos.V2.Naming.Dtos;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();




builder.Services.AddHttpClient();

//NACOS
builder.Host.ConfigureAppConfiguration(cbuilder =>
{
    cbuilder.AddJsonFile("appsettings.docker.json", optional: false, reloadOnChange: true);
});

var nacosconfig = builder.Configuration.GetSection("nacosConfig");

builder.Host.ConfigureAppConfiguration((context, builder) =>
{
    // add nacos
    builder.AddNacosConfiguration(nacosconfig);
});


builder.Services.AddDbContext<ContextDatabase>(options =>
    options.UseNpgsql(builder.Configuration["cn:postgres_retiros"]));
//End Nacos


//builder.Services.AddDbContext<ContextDatabase>(options =>
//    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<IServiceTransaction, ServiceTransaction>();
builder.Services.AddScoped<IServiceAccount, ServiceAccount>();
builder.Services.AddSingleton<IHttpClient, CustomHttpClient>();


//RabbitMQ

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));


builder.Services.AddRabbitMQ();
builder.Services.AddTransient<IRequestHandler<TransactionCreateCommand, bool>, TransactionCommandHandler>();

//Consul
builder.Services.AddSingleton<IServiceId, ServiceId>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddConsul();



//EndRabbitMQ

var app = builder.Build();

//Create Database

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    try
    {
        var context = services.GetRequiredService<ContextDatabase>();
        //Funcion static crea usuarios
        DbInitializer.Initialize(context);
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred creating the DB.");
    }
}

//Consult

var serviceId = app.UseConsul();
IHostApplicationLifetime applicationLifetime = app.Lifetime;
var consulClient = app.Services.GetRequiredService<IConsulClient>();
applicationLifetime.ApplicationStopped.Register(() =>
{
    consulClient.Agent.ServiceDeregister(serviceId);
});

//End Consult


// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();