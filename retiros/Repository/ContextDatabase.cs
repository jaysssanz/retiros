﻿using depositos.Models;
using Microsoft.EntityFrameworkCore;

namespace depositos.Repository
{
    public class ContextDatabase: DbContext
    {
        public ContextDatabase(DbContextOptions<ContextDatabase> options) : base(options)
        {
        }
        public DbSet<Transaction> Transaction { get; set; }
    }
}
