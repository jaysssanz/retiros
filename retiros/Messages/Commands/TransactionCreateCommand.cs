﻿using Cordillera.Distribuidas.Event.Commands;

namespace depositos.Messages.Commands
{
    public class TransactionCreateCommand : Command
    {
        public int IdTransaction { get; protected set; }
        public decimal Amount { get; protected set; }
        public string Type { get; protected set; } = null!;
        public string CreationDate { get; protected set; } = null!;
        public int AccountId { get; protected set; }

        public TransactionCreateCommand(int idTransaction, decimal amount, string type, string creationDate, int accountId)
        {
            IdTransaction = idTransaction;
            Amount = amount;
            Type = type;
            CreationDate = creationDate;
            AccountId = accountId;
        }
    }
}
