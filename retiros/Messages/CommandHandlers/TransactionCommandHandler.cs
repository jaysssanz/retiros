﻿using Cordillera.Distribuidas.Event.Bus;
using depositos.Messages.Commands;
using depositos.Messages.Events;
using MediatR;

namespace depositos.Messages.CommandHandlers
{
    public class TransactionCommandHandler : IRequestHandler<TransactionCreateCommand, bool>
    {
        private readonly IEventBus _bus;
        public TransactionCommandHandler(IEventBus bus)
        {
            _bus = bus;
        }

        public Task<bool> Handle(TransactionCreateCommand request, CancellationToken cancellationToken)
        {
            _bus.Publish(new TransactionCreatedEvent(
                   request.IdTransaction,
                   request.Amount,
                   request.Type,
                   request.CreationDate,
                   request.AccountId
               ));
            return Task.FromResult(true);

        }
    }
}
